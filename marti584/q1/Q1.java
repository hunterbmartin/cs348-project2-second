package org.myorg;
import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class Q1 {

  public static class Map 
    // mapper input key is always type LongWritable;
    // by looking at our input file, we know mapper value input is type Text;
    // we want mapper output key to be type Text;
    // we want mapper output value to be type Text;
    extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> 
  {
    private Text outKey = new Text();
    private Text outValue = new Text();

    public void map(LongWritable key, Text value, 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
    {
      String[] elements = value.toString().split("::");
      if(elements[1].contains("(1990)")){
        outKey.set(elements[0] + "::" + elements[1] + "::" + elements[2]);
        outValue.set("MoviesTable");
        output.collect(outKey, outValue);
      }
    }
  }

  public static class Reduce 
    extends MapReduceBase implements Reducer<Text, Text, Text, Text> 
  {
    private Text outKey = new Text();
    private Text outValue = new Text();
      
    public void reduce(Text key, Iterator<Text> values, 
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException 
    {
      // in this query, we expect that each reduce() call will only receive
      // one key-value pair, since rows from movies.dat are distinct;
      // however, in most MapReduce jobs, each reduce() call sees all of the
      // key-value pairs that match on a given key;
      // the code below demonstrates how to loop over multiple key-value pairs,
      // if they exist;

      String[] keyElements = key.toString().split("::");
      while(values.hasNext()){
        String[] valueElements = values.next().toString().split("::");
        if(true){
          outKey.set(keyElements[0]+"::"+keyElements[1]+"::"+keyElements[2]);
          outValue.set(valueElements[0]);
          output.collect(outKey, outValue);
        }
      }
    }
  }

  public static void main(String[] args) throws Exception {

    JobConf conf = new JobConf(Q1.class);
    conf.setJobName("Movies");

    // which HDFS file should we read?
    // which HDFS file should we write to?
    FileInputFormat.setInputPaths(conf, new Path(args[0]));
    FileOutputFormat.setOutputPath(conf, new Path(args[1]));

    // input/output format could be one of:
    // SequenceFileInputFormat, TextInputFormat, KeyValueTextInputFormat, etc.;
    // not important for this lab;
    conf.setInputFormat(TextInputFormat.class);
    conf.setOutputFormat(TextOutputFormat.class);

    // what will be the types of the keys/values that our
    // mappers and reducers output?
    conf.setOutputKeyClass(Text.class);
    conf.setOutputValueClass(Text.class);

    conf.setMapperClass(Map.class);
    conf.setReducerClass(Reduce.class);

    JobClient.runJob(conf);
  }
}


