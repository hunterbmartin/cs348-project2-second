package org.myorg;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class Q3 {

  public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {
    private IntWritable one = new IntWritable(1);
    private Text uid = new Text();

    public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
      String line = value.toString();
      String[] elements = line.split("::");

      uid.set(elements[0]);
      if(elements[2].equals("25")){
          output.collect(new Text(elements[3]), one);
      }
    }
  }

  public static class Reduce extends MapReduceBase implements Reducer<Text, IntWritable, Text, Text> {
    public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
      // because we want only distinct values, we will output
      // at most one key-value pair;
      int sum = 0;
      // String[] line = key.toString().split("::");
      // Map<String, Integer> m = new HashMap<String, Integer>();
      while (values.hasNext()) {
        // m.put(line[0], Integer.parseInt(line[1]));
        sum += values.next().get();
      }
      output.collect(new Text(key + "::" + sum), new Text("UsersTable"));
    }
  }

  public static void main(String[] args) throws Exception {
    JobConf conf = new JobConf(Q3.class);
    conf.setJobName("Users");

    conf.setMapOutputKeyClass(Text.class);
    conf.setMapOutputValueClass(IntWritable.class);
    conf.setOutputKeyClass(Text.class);
    conf.setOutputValueClass(Text.class);

    conf.setMapperClass(Map.class);
    // conf.setCombinerClass(Reduce.class);
    conf.setReducerClass(Reduce.class);

    conf.setInputFormat(TextInputFormat.class);
    conf.setOutputFormat(TextOutputFormat.class);

    FileInputFormat.setInputPaths(conf, new Path(args[0]));
    FileOutputFormat.setOutputPath(conf, new Path(args[1]));

    JobClient.runJob(conf);
  }
}

